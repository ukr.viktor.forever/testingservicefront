import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { API_URL } from './app-injection-tokens';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDialogModule } from '@angular/material/dialog';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatRadioModule } from '@angular/material/radio';
import { environment } from 'src/environments/environment';
import { ACCESS_TOKEN_KEY } from './services/auth.service';
import { JwtModule } from '@auth0/angular-jwt';
import { HttpClientModule } from '@angular/common/http';
import { RegisterComponent } from './components/register/register.component';
import { MenuComponent } from './components/menu/menu.component';
import { TestingComponent } from './components/tests/testing/testing.component';
import { TestsComponent } from './components/tests/tests.component';
import { ConfirmComponent } from './components/tests/confirm/confirm.component';
import { MatTabsModule } from '@angular/material/tabs';
import { ResultsComponent } from './components/results/results.component';
import { ResultDialogComponent } from './components/tests/testing/resultdialog.component';
import { AuthGuardService } from './services/auth-guard.service';
import { CrtestComponent } from './components/tests/crtest/crtest.component';
import { MatCommonModule } from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';
import { MatOptionModule } from '@angular/material/core';
import { RoleGuardService } from './services/role-guard.service';
import { UsersComponent } from './components/users/users.component';


export function tokenGetter(){
  return localStorage.getItem(ACCESS_TOKEN_KEY);
}


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    MenuComponent,
    TestingComponent,
    TestsComponent,
    ConfirmComponent,
    ResultsComponent,
    ResultDialogComponent,
    CrtestComponent,
    UsersComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MatTabsModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MatRadioModule,
    MatCardModule,
    MatInputModule,
    MatButtonModule,
    MatTableModule,
    MatFormFieldModule,
    MatDialogModule,
    MatCheckboxModule,
    MatCommonModule,
    MatSelectModule,
    MatOptionModule,

    JwtModule.forRoot({
      config: {
        tokenGetter: () => {return localStorage.getItem("access_token")},
        allowedDomains: environment.tokenWhitelistedDomains
      }
    })
  ],
  providers: [
    AuthGuardService,
    RoleGuardService,
    {
    provide: API_URL,
    useValue: environment.api
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
