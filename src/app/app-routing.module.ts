import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { ResultsComponent } from './components/results/results.component';
import { CrtestComponent } from './components/tests/crtest/crtest.component';
import { TestingComponent } from './components/tests/testing/testing.component';
import { TestsComponent } from './components/tests/tests.component';
import { UsersComponent } from './components/users/users.component';
import { AuthGuardService as AuthGuard } from './services/auth-guard.service';
import { RoleGuardService as RoleGuard } from './services/role-guard.service';

const routes: Routes = [
  {path: '', component: LoginComponent},
  {path: 'Registration', component: RegisterComponent},
  {path: 'Testing/:id', component: TestingComponent, canActivate: [AuthGuard] },
  {path: 'Result', component: ResultsComponent, canActivate: [AuthGuard] },
  {path: 'Tests', component: TestsComponent, canActivate: [AuthGuard] },
  {path: 'Create', component: CrtestComponent, canActivate: [RoleGuard], data: { expectedRole: 'Admin' }},
  {path: 'Users', component: UsersComponent, canActivate: [RoleGuard], data: { expectedRole: 'Admin' }},
  {path: '**', component: LoginComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
