import { Component, OnInit } from '@angular/core';
import { UserViewModel } from 'src/app/models/userViewModel';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  users: UserViewModel[]=[];
  constructor(private userService:UserService) { }
  cTitle = ['UserName','Email','Roles','Admin']
  ngOnInit(): void {
    this.userService.getUsers().subscribe(res=>this.users = res)
  }
  isAdmin(i:number)
  {
    let user = this.users[i].roles.includes("Admin");
    return user;
  }
  makeAdmin(user:UserViewModel)
  {
    this.userService.addAdmin(user.userName).subscribe(res=> this.ngOnInit());
  }
}
