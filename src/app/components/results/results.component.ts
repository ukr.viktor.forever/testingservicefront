import { Component, OnInit } from '@angular/core';
import { ResultModel } from 'src/app/models/resultModel';
import { AuthService } from 'src/app/services/auth.service';
import { ResultService } from 'src/app/services/result.service';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss']
})
export class ResultsComponent implements OnInit {

  results: ResultModel[] = [];

  constructor(private service: ResultService, private as:AuthService) {

  }
  cTitle = ['Name','Description', "Correct answers"];
  ngOnInit(): void {
    this.service.getMyResults(this.as.getCurrentUserId()).subscribe(res=>{
      this.results = res
    })

  }

}
