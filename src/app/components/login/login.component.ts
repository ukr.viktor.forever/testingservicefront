import { Component, OnInit, Output, EventEmitter} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  constructor(private as: AuthService, private router: Router) {

  }

  signIn(email: string, password: string){
    this.as.signIn(email, password)
      .subscribe(res => {
        this.router.navigate([`/Tests`])
      }, error => {
        alert('wrong email or password');
      })
  }

}
