import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { TestModel } from 'src/app/models/testModel';
import { AuthService } from 'src/app/services/auth.service';
import { TestService } from 'src/app/services/test.service';
import { ConfirmComponent } from './confirm/confirm.component';

@Component({
  selector: 'app-tests',
  templateUrl: './tests.component.html',
  styleUrls: ['./tests.component.scss']
})
export class TestsComponent implements OnInit {

  tests:TestModel[] = [];
  confirmModel: TestModel = {
    id:0,
    name:"",
    description:""
  };
  cTitle = ['№','Name','Description','Start']
  constructor(private servise: TestService,  public dialog: MatDialog, private as: AuthService) { }

  ngOnInit(): void {
    this.servise.getMyTests().subscribe(res=>this.tests = res)
  }

  confirmStarting(test:TestModel): void {
    this.confirmModel = test;
    this.dialog.open(ConfirmComponent, {
      width: '400px',
      height: '300px',
      data: this.confirmModel
    });
  }
}
