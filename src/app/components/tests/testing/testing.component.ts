import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TestingModel } from 'src/app/models/testingModel';
import { TestService } from 'src/app/services/test.service';
import { ResultService } from 'src/app/services/result.service';
import { MatDialog } from '@angular/material/dialog';
import { ResultDialogComponent } from './resultdialog.component';
import { AuthService } from 'src/app/services/auth.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-testing',
  templateUrl: './testing.component.html',
  styleUrls: ['./testing.component.scss']
})
export class TestingComponent implements OnInit {

  test:TestingModel = {
    id: 0,
    name: "",
    description : "",
    questions : [
  {
        text: "",
        answersText: [],
        answerIds:[]
    }

    ]
  };

  answerForm = new FormGroup(
    {
      answer: new FormControl('', [
        Validators.required
      ])
    }
  );

  resultArray:string[] = [];
  confirmedResult:boolean[][]=[];
  testId:number = 0;
  buttonClicked:boolean[]=[];
  correct:Number = 0
  demoTabIndex:number=0;

  constructor(private servise:TestService, private route: ActivatedRoute,private us: AuthService, private resService: ResultService, private dialog: MatDialog) {
    this.testId = route.snapshot.params['id'];
    this.resize();
   }
   ngOnInit(): void {
    this.servise.getTesting(this.testId).subscribe(res => {
      this.test = res;
      this.resize();
    });
  }

   resize()
   {
    this.confirmedResult = new Array(this.test.questions.length);
    for(let i = 0; i < this.test.questions.length; i++)
    {
      this.confirmedResult[i] = new Array(this.test.questions[i].answerIds.length).fill(false);
    }
   }

   confirm(ind:number)
   {
    this.buttonClicked[ind] = true;
    this.demoTabIndex++;
    let answerIds = this.test.questions[ind].answerIds;
    let answerId = answerIds.findIndex((value)=>value == this.answerForm.value.answer);
    this.confirmedResult[ind][answerId] = true;
    this.resultArray.push(this.answerForm.value.answer);
   }

  finish(){
    let userId=this.us.getCurrentUserId();
    this.resService.addResult(this.resultArray, this.test.id, userId).subscribe(
      res=>{this.correct = res,
      this.dialog.open(ResultDialogComponent, {
        width: '200px',
        data: this.correct
    }
    )

  });

  }
}
