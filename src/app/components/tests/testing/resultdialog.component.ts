import { Component, Inject } from "@angular/core";
import { MAT_DIALOG_DATA } from "@angular/material/dialog";

@Component({
  selector: 'finish-dialog',
  templateUrl: 'result.html',
})
export class ResultDialogComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public data: number) {}
}
