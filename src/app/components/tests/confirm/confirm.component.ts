import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { TestModel } from 'src/app/models/testModel';



@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss']
})
export class ConfirmComponent {
  isChecked = false;
  constructor(
    public dialogRef: MatDialogRef<ConfirmComponent>,
    @Inject(MAT_DIALOG_DATA) public data: TestModel,
    private router: Router
  ) {}

  onClick(): void{
    if(this.isChecked){
      this.dialogRef.close();
      this.router.navigate([`/Testing/${this.data.id}`])
    }
  }
}
