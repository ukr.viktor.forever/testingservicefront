import { Component, OnInit } from '@angular/core';
import { Answer, Question, TestAddModel } from 'src/app/models/testAddModel';
import { TestService } from 'src/app/services/test.service';
import { UserService } from 'src/app/services/user.service';


@Component({
  selector: 'app-crtest',
  templateUrl: './crtest.component.html',
  styleUrls: ['./crtest.component.scss']
})
export class CrtestComponent implements OnInit {
  users: string[];
  _answer: Answer;

  _question: Question;

  test: TestAddModel;

  constructor(private userService: UserService, private testService: TestService) {
    this.users = [];
    this._answer = {
      text: "",
      isCorrect: false
    };
    this._question = {
      text: "",
      answers: [this._answer],
    };
    this.test =
    {
      name: "",
      description: "",
      allowedUsers: [],
      questions: [
        this._question
      ]
    };

  }

  ngOnInit(): void {
    this.userService.getUserNames().subscribe(res => this.users = res);
  }

  send() {
    this.testService.addTest(this.test).subscribe();
    this._answer = {
      text: "",
      isCorrect: false
    };
    this._question = {
      text: "",
      answers: [this._answer],
    };
    this.test =
    {
      name: "",
      description: "",
      allowedUsers: [],
      questions: [
        this._question
      ]
    };
  }
  addNewQuestion() {
    let answer: Answer = {
      text: "",
      isCorrect: false
    }
    let question: Question = {
      text: "",
      answers: [answer]
    }
    this.test.questions.push(question);
  }
  addNewAnswer(i: number) {
    let n: Answer = {
      text: "",
      isCorrect: false
    }
    this.test.questions[i].answers.push(n);
  }

  check(i: number, j: number) {
    for (const ans of this.test.questions[i].answers) {
      ans.isCorrect = false;
    }
    this.test.questions[i].answers[j].isCorrect = true;
  }


}
