import { Component} from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {

  constructor(private as: AuthService, private router: Router) {
  }

  signUp(email: string, password: string, userName: string){
    this.as.signUp(email, password, userName)
      .subscribe(res => {
        this.router.navigate([` `])
      }, err=>console.log(err)
      );
  }

}
