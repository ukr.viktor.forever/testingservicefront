import { Component } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent {

  constructor(private as: AuthService) { }
  signOut(){
    this.as.signOut()
  }
  isAdmin(){
    let roles = this.as.getCurrentRoles();
    if(roles.includes("Admin"))
    {
      return true;
    }else
    {
      return false;
    }
  }


}
