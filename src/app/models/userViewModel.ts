export interface UserViewModel {
  userName: string;
  roles: string[];
  email: string;
}
