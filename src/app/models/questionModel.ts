export interface QuestionModel{
  text: string;
  answersText: string[];
  answerIds: string[];
}
