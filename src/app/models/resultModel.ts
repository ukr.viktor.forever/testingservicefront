export interface ResultModel{
  correctAnswers: number;
  testName: string;
  testDescription: string;
}
