export interface TestAddModel{
  name:string;
  description:string;
  allowedUsers:string[];
  questions:Question[];
}
export interface Question{
  text:string;
  answers:Answer[];
}
export interface Answer{
  text:string;
  isCorrect: boolean;
}
