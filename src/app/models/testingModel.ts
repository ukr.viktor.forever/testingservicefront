import { QuestionModel } from "./questionModel";

export interface TestingModel{
  id:number;
  name: string;
  description : string;
  questions : QuestionModel[];
}
