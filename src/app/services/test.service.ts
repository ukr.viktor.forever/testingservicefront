import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable, tap } from 'rxjs';
import { API_URL } from '../app-injection-tokens';
import { TestingModel } from '../models/testingModel';
import { TestModel } from '../models/testModel';
import { TestAddModel } from '../models/testAddModel';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class TestService {
  private Url = `${this.apiUrl}api/Test`;

  constructor(private http: HttpClient, @Inject(API_URL) private apiUrl: string, private auth: AuthService) {

  }
  getMyTests(): Observable<TestModel[]>{
    return this.http.get<TestModel[]>(`${this.Url}/${this.auth.getCurrentUserId()}`);
  }
  getTesting(testId: number): Observable<TestingModel>{
    return this.http.get<TestingModel>(`${this.Url}/start/${testId}`);
  }
  addTest(test:TestAddModel){
    return this.http.post(`${this.Url}`, test);
  }
}
