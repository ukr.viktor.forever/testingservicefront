import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot} from '@angular/router';
import { AuthService } from './auth.service';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable()
export class RoleGuardService implements CanActivate {
  constructor(public auth: AuthService, public router: Router,private jwtHelper: JwtHelperService)
  {

  }

  canActivate(route: ActivatedRouteSnapshot): boolean {
    const expectedRole = route.data['expectedRole'];
    const roles = this.auth.getCurrentRoles();
    if (
      !this.auth.isAuthenticated() ||
      !roles.includes(expectedRole)
    ) {
      this.router.navigate(['']);
      return false;
    }
    return true;
  }
}
