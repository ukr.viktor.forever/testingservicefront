import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { API_URL } from '../app-injection-tokens';
import { ResultModel } from '../models/resultModel';

@Injectable({
  providedIn: 'root'
})
export class ResultService {
  private Url = `${this.apiUrl}api/Result`;

  constructor(private http: HttpClient, @Inject(API_URL) private apiUrl: string) {

  }
  addResult(result: string[], testId:number, userId:string):Observable<number>
  {
    return this.http.post<number>(`${this.Url}/${testId}/${userId}`,result );
  }
  getMyResults(userId:string) :Observable<ResultModel[]>
  {
    return this.http.get<ResultModel[]>(`${this.Url}/${userId}`);
  }
}
