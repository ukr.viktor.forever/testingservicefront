import { HttpClient } from '@angular/common/http';
import { Token } from '../models/token';
import { Inject, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Observable, tap } from 'rxjs';
import { API_URL } from '../app-injection-tokens';
import { UserLoginModel } from '../models/userLoginModel';
import { UserRegisterModel } from '../models/userRegisterModel';
import { tokenGetter } from '../app.module';

export const ACCESS_TOKEN_KEY = 'access_token';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  uLogin : UserLoginModel;
  uRegister : UserRegisterModel;

  constructor(
    private http: HttpClient,
    @Inject(API_URL) private apiUrl: string,
    private jwtHelper: JwtHelperService,
    private router: Router
  ) {
    this.uLogin = {email:"", password:""}
    this.uRegister = {email:"", password:"", userName:""}
  }

  signIn(email: string, password: string): Observable<Token> {
    this.uLogin = {
      email : email,
      password : password
    }
    return this.http.post<Token>(
      `${this.apiUrl}api/Login/SignIn`, this.uLogin)
      .pipe(
      tap(token => {
        localStorage.setItem(ACCESS_TOKEN_KEY, token.access_token);
      })
    )
  }
  signUp(email: string, password: string, userName: string) {
    this.uRegister ={
      email: email,
      password: password,
      userName: userName
    }
    return this.http.post(`${this.apiUrl}api/Login/SignUp`, this.uRegister)
  }
  getCurrentRoles() :string
  {
    const token: any = tokenGetter() ?? "";
    const tokenPayload = this.jwtHelper.decodeToken(token);
    const roles:string = tokenPayload.role.toString();
    return roles;
  }
  getCurrentUserId() :string
  {
      let tok = tokenGetter() ?? "";
      const decoded = this.jwtHelper.decodeToken(tok)?.sub ?? "Unauthorized";

    return decoded;
  }
  isAuthenticated(): boolean {
    var token = localStorage.getItem(ACCESS_TOKEN_KEY);
    var res = token != null && !this.jwtHelper.isTokenExpired(token);
    return res;
  }
  signOut(): void{
    localStorage.removeItem(ACCESS_TOKEN_KEY);
    this.router.navigate(['']);
  }
}
