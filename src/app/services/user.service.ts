import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { API_URL } from '../app-injection-tokens';
import { UserViewModel } from '../models/userViewModel';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private Url = `${this.apiUrl}api/User`;

  constructor(private http: HttpClient, @Inject(API_URL) private apiUrl: string) {

  }
  getUserNames(): Observable<string[]>{
    return this.http.get<string[]>(`${this.Url}/UserNames`);
  }
  getUsers(): Observable<UserViewModel[]>{
    return this.http.get<UserViewModel[]>(`${this.Url}`);
  }
  addAdmin(userName:string){
    let data = {
      name: userName,
      role: "Admin"
    }
    return this.http.put(`${this.apiUrl}api/Role`,data );

  }

}
